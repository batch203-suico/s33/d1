// [SECTION] JavaScript Synchrounous vs Asynchrounous

// JavaScript is by default synchrounous meaning that it only executes on statements at a time.

console.log(`Hello World`);
// conosle.log(`Hello Again`);
console.log(`Good Bye`);
// Code blocking - waiting for the specific statement to finish before the next statement 
// for(let i=0; i<1500; i++){
// 	console.log(i);
// }

console.log(`Hello Again`);

// Asynchronous means that we can proceed to exeecute other statements, while time consuming code is running in the background.

// [SECTION] Getting all posts

// The Fetch API that allows us to asynchronously request for resource (data).
	// "fetch()" method in JavaScript is used to request ti the server and load information on the webpages.
	// Syntax:
		// fetch("apiURL")
	// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value.
		// A promise may be in one of 3 possible states: fullfilled, rejected, or pending.
		/*
			pending: initial states, neither fulfilled nor rejected response.
				fulfilled: operation was completed 
				rejected: operation failed.

			Syntax:
				fetch("apiURL")
				.then((response)=>{});
		*/
	fetch("https://jsonplaceholder.typicode.com/posts")
	// The ".then" methods catpturers the response object and returns another promise which will be either "resolved" or "rejected" 
	// .then(response => console.log(response.status));

	// "json()" method will convert JSON format to JavaScript object
	.then(response => response.json())
	// We can now manipulate or use the converted response in our code.
	.then(json => console.log(json));
	// We can use array methods to dispaly each post title.
	// .then(json => {
	//  	json.forEach(post => console.log(post.title));
	// })

	// The "async" and "await" keyword to achieve asynchronous code.

	async function fetchData(){
		let result = await(fetch("https://jsonplaceholder.typicode.com/posts"));
		// returned the "response" object
		console.log(result);

		let json = await result.json();

		console.log(json);

	}

	fetchData();

// [SECTION] Getting a specific post 
// Retrieves a specific post following the Rest API (retrieve, /posts/:id, GET)
	// ":id" is a wildcard where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL

fetch("https://jsonplaceholder.typicode.com/posts/1")
.then(response => response.json())
.then(json => console.log(json))

// [SECTION] Creating a post

/*
		Syntax:
			fetch("apiURL", {options})
			.then((response) =>{})
			.then((response) =>{})
	*/

	fetch("https://jsonplaceholder.typicode.com/posts",
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					title: "New Post",
					body: "Hello World!",
					userId: 1
				})
			}
		)
	.then(response => response.json())
	.then(json => console.log(json));

	// [SECTION] Updating a post

	fetch("https://jsonplaceholder.typicode.com/posts/1",{
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "updated post",
			body: "Hello again",
			userId: 1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json));

	// [SECTION] Updating a post using PATCH method
	// PUT vs PATCH
		// PATCH is used to update a single/several properties
		// PUT is used to update the whole document/object
	fetch("https://jsonplaceholder.typicode.com/posts/1",{
		method: "PATCH",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "Corrected post title"
		})
	})
	.then(response => response.json())
	.then(json => console.log(json));

	// [SECTION] Delete a post

	fetch("https://jsonplaceholder.typicode.com/posts/1", {method: "DELETE"})

	.then(response => response.json())
	.then(json => console.log(json))

